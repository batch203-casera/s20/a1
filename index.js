// Solution 1:

let number = prompt("Give me a number: ");

number = parseInt(number, 10);

for(let x = number; x >= 0; x--) {

	if(x <= 50){

		console.log("The current value is at 50. Terminating the loop");
		break;

	}

	if(x % 10 === 0){

		console.log("The number is divisible by 10. Skipping the number.");
		continue;

	}

	if(x % 5 === 0){

		console.log(x);

	}

}

let word = 'supercalifragilisticexpialidocious';
let newWord = ''
console.log(word);

for(let i = 0; i < word.length ; i++){
	if( word[i] === 'a' ||
		word[i] === 'e' ||
		word[i] === 'i' ||
		word[i] === 'o'||
		word[i] === 'u'){
		continue;
	}

	newWord += word[i];

}

console.log(newWord);
